<?php

namespace App\Controller;

use App\Form\CoolType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PublicController extends AbstractController {
  #[Route(
    path: '/',
    name: 'public_homepage',
    methods: ['GET', 'POST'],
  )]
  public function homepage(Request $request): Response
  {
    $form = $this
      ->createForm(CoolType::class)
      ->handleRequest($request);

    if ($form->isSubmitted() && $form->isValid()) {
      $firstname = $form->get('firstname')->getData();
      $lastname = $form->get('lastname')->getData();
      $email = $form->get('email')->getData();
      $phone = $form->get('phone')->getData();

      return $this->redirectToRoute('public_about');
    }

    $username = $request->query->get('q') ?? 'default';

    return $this->render('homepage.html.twig', [
      'username' => $username,
      'form' => $form,
    ]);
  }

  #[Route(path: '/about', name: 'public_about', methods: ['GET'])]
  public function about(): Response
  {
    return new Response('About page', Response::HTTP_CREATED);
  }

  public function isPalindrome(string $text): bool
  {
    return $text === strrev($text);
  }
}
