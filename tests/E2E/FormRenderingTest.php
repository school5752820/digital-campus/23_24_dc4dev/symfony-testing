<?php

namespace App\Tests\E2E;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;

class FormRenderingTest extends WebTestCase
{
    public function testSomething(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorExists('input[name="cool[firstname]"]');
    }

    public function testSubmission(): void
    {
        $client = self::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertResponseIsSuccessful();

        $form = $crawler->selectButton('cool_submit')->form();
        $form->setValues([
            'cool[firstname]' => 'Hannah',
            'cool[lastname]' => 'Montana',
            'cool[email]' => 'hannah@montana.com',
            'cool[phone]' => '0000000000',
        ]);

        $client->submit($form);

        $this->assertResponseRedirects('/about', 302);
        $client->followRedirect();
        $this->assertResponseStatusCodeSame(Response::HTTP_CREATED);
    }
}
