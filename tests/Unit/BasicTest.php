<?php

namespace App\Tests\Unit;

use App\Controller\PublicController;
use PHPUnit\Framework\TestCase;

class BasicTest extends TestCase
{
    public function testShouldBePalindrome(): void
    {
        $controller = new PublicController();
        $this->assertTrue($controller->isPalindrome('kayak'));
    }
    
    public function testShouldNotBePalindrome(): void
    {
        $controller = new PublicController();
        $this->assertFalse($controller->isPalindrome('bonjour'));
    }
}
